import ply
import ply.lex as lex

tokens = (
    'TXT', #TEXTE
    'ACO', #ACCOLADE OUVERTE
    'ACC', #ACCOLADE FERMEE
    'BRO', #PARENTHESE OUVERTE
    'BRC', #PARENTHESE FERMEE
    'PRT', #PRINT
    'FOR', #FOR
    'IN', #IN
    'DO', #DO
    'EF', #END FOR
    'PV', #POINT VIRGULE
    'PT', #POINT
    'ASN', #ASSIGNATION
    'VIR', #VIRGULE
    'VAR', #VARIABLE
    'STRI', #STRING IN
    'STRO', #STRING OUT
    'STR', 
    'ADDOP', #OPERATION + ET - 
    'MULOP', #OPERATION / ET *
    'INT', #ENTIER
    'IF', #IF
    'ENDIF', #END IF
    'TRUE', #BOOLEEN VRAI
    'FALSE', #BOOLEE FAUX
    'OR', #OPERATEUR OU LOGIQUE
    'AND', #OPERATEUR ET LOGIQUE
    'EQ', #OPERATEUR D'ÉGALITÉ
    'REL', #OPERATEUR RELATIONNEL
)

#’[\w ; & < > " _ \- \. \\ / \n :   \,]+’

states=(
    ('inDumboBloc','exclusive'),
    ('inString','exclusive'),
)

t_TXT=r'[\w;&<>"_\-\.\\/\n: \,\t\r\v]+'
t_inDumboBloc_PV=';'
t_inDumboBloc_PT=r'\.'
t_inDumboBloc_ADDOP=r'\+|-'
t_inDumboBloc_MULOP=r'\*|/'
t_inDumboBloc_EQ=r'==|!='
t_inDumboBloc_REL=r'>|<|<=|>='

def t_inDumboBloc_STRI(t):
    r'(’|\')'
    t.lexer.begin('inString')
    return t

def t_inString_STRO(t):
    r'(’|\')'
    t.lexer.begin('inDumboBloc')
    return t

t_inString_STR=r'[\w ; & < > = " _ \- \. \\ / \n :   \,]+'

def t_ACO(t):
    r'\{\{'
    t.lexer.begin('inDumboBloc')
    return t

def t_inDumboBloc_PRT(t):
    r'print'
    return t

def t_inDumboBloc_FOR(t):
    r'for'
    return t

t_inDumboBloc_ASN=r':='
def t_inDumboBloc_IN(t):
    r'in'
    return t

def t_inDumboBloc_DO(t):
    r'do'
    return t

def t_inDumboBloc_EF(t):
    r'endfor'
    t.lexer.begin('inDumboBloc')
    return t

def t_inDumboBloc_IF(t):
    r'if'
    return t
def t_inDumboBloc_ENDIF(t):
    r'endif'
    return t

def t_inDumboBloc_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_inDumboBloc_TRUE(t):
    r'true'
    return t

def t_inDumboBloc_FALSE(t):
    r'false'
    return t

def t_inDumboBloc_OR(t):
    r'or'
    return t

def t_inDumboBloc_AND(t):
    r'and'
    return t

def t_inDumboBloc_VAR(t):
    r'\w+'
    return t

def t_inDumboBloc_ACC(t):
    r'\}\}'
    t.lexer.begin('INITIAL')
    return t

def t_inDumboBloc_BRO(t):
    r'\('
    return t

t_inDumboBloc_VIR=r','

def t_inDumboBloc_BRC(t):
    r'\)'
    return t

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

t_inDumboBloc_ignore=' \t\n'

def t_error(t):
    print("Illegalcharacter '%s'" %t.value[0])
    t.lexer.skip(1)

def t_inDumboBloc_error(t):
    print("Illegalcharacter '%s'" %t.value[0])
    t.lexer.skip(1)

def t_inString_error(t):
    print("Illegalcharacter '%s'" %t.value[0])
    t.lexer.skip(1)

lexer=lex.lex()

if __name__ == "__main__":
    import sys
    #lexer = lex.lex()
    lexer.input(sys.stdin.read())
    for token in lexer:
        print("line %d : %s (%s)" %(token.lineno, token.type, token.value))