from classes import liste,atom

def listToText(t):
    s=""
    if type(t)!=list:
        return str(t)
    else:
        for i in t:
            s+=listToText(i)
    return s

def forTransform(t):
    if type(t)==list:
        res=[]
        for i in t:
            res.append(atom(obj='str',value=i))
        return res
    else:
        return atom(obj='var',name=t)

def boolSyn(p_bool):
    if p_bool == 'true':
        return True
    elif p_bool == 'false':
        return False

operations = {
    '+': lambda x, y: x+y,
    '-': lambda x, y: x-y,
    '*': lambda x, y: x*y,
    '/': lambda x, y: x/y,
    '==': lambda x, y: x==y,
    '!=': lambda x, y: x!=y,
    '<=': lambda x, y: x<=y,
    '>=': lambda x, y: x>=y,
    '>': lambda x, y: x>y,
    '<': lambda x, y: x<y,
    'and': lambda x, y: x and y,
    'or': lambda x, y: x or y,
}

#               EVALUATIONS
def forEval(params, datas, tempVars):
    p0=forTransform(params[0])
    p1=forTransform(params[1])
    tempVars.fData(p0)
    result = []
    for i in pEval(p1, datas, tempVars):
        tempVars.setValue(p0, i)
        result.append(pEval(params[2], datas, tempVars))
    tempVars.removeLast()
    return result

def printEval(params, datas, tempVars):
    return pEval(params[0], datas, tempVars)

def asnEval(params, datas, tempVars):
    datas.fData(params[0], pEval(params[1], datas, tempVars))

def ifEval(params, datas, tempVars):
    result = []
    if pEval(params[0], datas, tempVars):
        result.append(pEval(params[1], datas, tempVars))
    return result

def concatEval(params, datas, tempVars):
    a = str(pEval(params[0], datas, tempVars))
    b = str(pEval(params[1], datas, tempVars))
    return a+b

def binrEval(e, datas, tempVars):
    params = e.parameters
    a = pEval(params[1], datas, tempVars)
    b = pEval(params[2], datas, tempVars)
    return operations[params[0]](a, b)

def fctEval(e, datas, tempVars):
    '''
    (for, print, if, concat)
    '''
    if e.name=="for":
        return forEval(e.parameters, datas, tempVars)
    elif e.name=="print":
        return printEval(e.parameters, datas, tempVars)
    elif e.name=='concat':
        return concatEval(e.parameters, datas, tempVars)
    elif e.name=='if':
        return ifEval(e.parameters, datas, tempVars)

def varEval(e, datas, tempVars):
    result = tempVars.getValue(e)
    if not(result):
        result = datas.getValue(e)
    return result

def pEval(template, datas, tempVars=liste()):
    result = []
    if template!=None:
        if not(type(datas) == type(liste())):
            datas = liste(datas)
        '''
        text, var, str, function, int, bool
        '''
        if type(template) == list:
            for e in template:
                if e.obj == 'txt':
                    result.append(e.value)
                elif e.obj == 'function':
                    a = fctEval(e, datas, tempVars)
                    result.append(a)
                elif e.obj == 'asn':
                    asnEval(e.parameters, datas, tempVars)
                elif e.obj == 'binr':
                    result.append(binrEval(e, datas, tempVars))
                elif e.obj == 'str':
                    result.append(e.value)
                elif e.obj == 'var':
                    a = varEval(e, datas, tempVars)
                    result.append(a)
                elif e.obj == 'bool':
                    result.append(e.value)
                elif e.obj == 'int':
                    result.append(e.value)
        else:
            if template.obj == 'str':
                return template.value
            elif template.obj == 'function':
                return fctEval(template, datas, tempVars)
            elif template.obj == 'var':
                a = varEval(template, datas, tempVars)
                return a
            elif template.obj == 'bool':
                return template.value
            elif template.obj == 'int':
                return template.value
            elif template.obj == 'binr':
                return binrEval(template, datas, tempVars)
    return result