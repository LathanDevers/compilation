class liste:
    def __init__(self, vars=[]):
        self.vars=vars

    def removeLast(self):
        self.vars.pop()

    def addVar(self,var, value=None):
        self.vars.append(var)
    

    def fData(self,node,data=None):
        l=self.getNames()
        if node.name in l:
            self.vars[self.getNames().index(node.name)].setValue(data)
        else:
            self.vars.append(atom(obj="var",name=node.name,value=data))

    def getValue(self,atom):
        """Récupère la valeure de l'atome dans la liste des variables
        
        Args:
            atom (atom): l'atome à retrouver
        
        Returns:
            bool, int, string: retourne la valeur de la variable (None si la valeur n'existe pas)
        """
        for i in self.vars:
            if i.obj=="var" and i.name==atom.name:
                return i.value
            elif i.obj == "asn" and i.parameters[0].name == atom.name:
                return i.parameters[1]
        return None

    def getNames(self):
        temp=[]
        for var in self.vars:
            temp.append(var.name)
        return temp


    def setValue(self,atom,_value):
        for i in self.vars:
            if i.obj=="var" and i.name==atom.name:
                i.value=_value
                return True
        return False
    
    def __str__(self):
        s=""
        for i in self.vars:
            s+=str(i)
            s+="; "
        return s
    

class atom:
    def __init__(self,obj="Object",name=None,value=None,parameters=None):
        """Intialisation
        
        Args:
            obj (str, optional): Type de l'atome (text, var, str, function, binr, asn, int, bool). Defaults to "Object".
            name (str, optional): Nom de l'atome (for, print, asn, if, rel, op, bool, concat). Defaults to None.
            value (str, optional): Ce que va retourner l'évaluation de l'atome. Defaults to None.
            parameters (list, optional): Paramètres de la fonction d'évaluation. Defaults to None.
        """
        self.obj=obj
        self.name=name
        self.value=value
        self.parameters=[]
        if parameters!=None and len(parameters) != 0:
            self.parameters = parameters

    def add_params(self,parameters):
        self.parameters.append(parameters)
    
    def setValue(self,value):
        self.value=value

    def __str__(self):
        s=""
        s+="obj : "+str(self.obj)
        s+=" ; "
        s+="name : "+str(self.name)
        s+=" ; "
        s+="value : "+str(self.value)
        if len(self.parameters)!=0:
            s+="parameters : ("
            for i in self.parameters:
                s+="\t"
                s+=str(i)
            s+=")"
        return s