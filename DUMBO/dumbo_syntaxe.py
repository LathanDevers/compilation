import ply.yacc as yacc
from dumbo_lex import tokens
import jfrag



#####################    PROGRAMME    #####################

#     programme <- txt
def p_programme_a(p):
    '''programme : TXT'''
    p[0] = [jfrag.atom(obj = 'txt', value = p[1])]

#     programme <- txt programme
def p_programme_b(p):
    '''programme : TXT programme'''
    p[0]=[jfrag.atom(obj = 'txt', value = p[1])]+p[2]

#     programme <- dumbo_bloc
def p_programme_c(p):
    '''programme : dumbobloc'''
    p[0]=p[1]

#     programme <- dumbo_bloc programme
def p_programme_d(p):
    '''programme : dumbobloc programme'''
    p[0]=p[1]+p[2]

#####################    DUMBO BLOC    #####################

#     dumbobloc <- {{ expression_list }}
def p_dumbobloc_a(p):
    '''dumbobloc : ACO expressionlist ACC'''
    p[0]=p[2]

#####################    EXPRESSION    #####################

#     expression <- variable := string_list
#     expression <- variable := string_expression
def p_expression_a(p):
    '''expression : VAR ASN stringlist
                  | VAR ASN stringexpression'''
    a = jfrag.atom(obj='var', name=p[1])
    p[0] = jfrag.atom(obj='asn',parameters=[a, p[3]])

#     expression <- print string_expression
def p_expression_b(p):
    '''expression : PRT stringexpression'''
    p[0]=jfrag.atom(obj='function', name='print', parameters=[p[2]])

#     expression <- for variable in string_list do expression_list endfor
#     expression <- for variable in variable do expression_list endfor
def p_expression_c(p):
    '''expression : FOR VAR IN stringlist DO expressionlist EF
                  | FOR VAR IN VAR DO expressionlist EF'''
    p[0]=jfrag.atom(obj='function', name='for', parameters=[p[2], p[4], p[6]])

def p_expression_d(p):
    '''expression : IF conditionalOrExpression DO expressionlist ENDIF'''
    p[0] = jfrag.atom(obj='function', name='if', parameters=[p[2], p[4]])

def p_expression_e(p):
    '''expression : VAR ASN conditionalOrExpression'''
    a = jfrag.atom(obj='var', name=p[1])
    p[0] = jfrag.atom(obj='asn',parameters=[a, p[3]])

def p_expression_cond(p):
    """expression : conditionalOrExpression"""
    p[0] = p[1]

#####################  BOOLEAN EXPRESSION   ####################

def p_conditionalOrExpression_a(p):
    '''conditionalOrExpression : conditionalAndExpression'''
    p[0] = p[1]

def p_conditionalOrExpression_b(p):
    '''conditionalOrExpression : conditionalOrExpression OR conditionalAndExpression'''
    p[0] = jfrag.atom(obj='binr', name='bool', parameters=['or', p[1], p[3]])

def p_conditionalAndExpression_a(p):
    '''conditionalAndExpression : equalityExpression'''
    p[0] = p[1]

def p_conditionalAndExpression_b(p):
    '''conditionalAndExpression : conditionalAndExpression AND equalityExpression'''
    p[0] = jfrag.atom(obj='binr', name='bool', parameters=['and', p[1], p[3]])

def p_equalityExpression_a(p):
    '''equalityExpression : relationalExpression'''
    p[0] = p[1]

def p_equalityExpression_b(p):
    '''equalityExpression : equalityExpression EQ relationalExpression'''
    p[0] = jfrag.atom(obj='binr', name='op', parameters=['=', p[1], p[3]])

def p_relationalExpression_a(p):
    '''relationalExpression : opexpression'''
    p[0] = p[1]

def p_relationalExpression_b(p):
    '''relationalExpression : relationalExpression REL opexpression'''
    p[0] = jfrag.atom(obj='binr', name='rel', parameters=[p[2], p[1], p[3]])

def p_opexpression_a(p):
    """opexpression : opexpression ADDOP mulexpression"""
    p[0] = jfrag.atom(obj='binr', name='op', parameters=[p[2], p[1], p[3]])

def p_opexpression_b(p):
    """opexpression : mulexpression"""
    p[0] = p[1]

def p_mulexpression_a(p):
    """mulexpression : mulexpression MULOP facexpression"""
    p[0] = jfrag.atom(obj='binr', name='op', parameters=[p[2], p[1], p[3]])

def p_mulexpression_b(p):
    """mulexpression : facexpression"""
    p[0] = p[1]

def p_facexpression_var(p):
    '''facexpression : VAR'''
    p[0] = jfrag.atom(obj='var', name=p[1])

def p_facexpression_int(p):
    """facexpression : INT"""
    p[0] = jfrag.atom(obj='int', value=p[1])

def p_facexpression_true(p):
    '''facexpression : TRUE
                     | FALSE'''
    p[0] = jfrag.atom(obj='bool', value=jfrag.boolSyn(p[1]))

#####################    EXPRESSION LIST    #####################

#     expression_list <- expression ;
def p_expressionlist_a(p):
    '''expressionlist : expression PV'''
    p[0]=[p[1]]

#     expression_list <- expression ; expression_list
def p_expressionlist_b(p):
    '''expressionlist : expression PV expressionlist'''
    p[0]=[p[1]]+p[3]

#####################    STRING EXPRESSION    #####################

#     f_expression <- string
def p_fexpression_a(p):
    '''fexpression : STRI STR STRO'''
    p[0]=jfrag.atom(obj='str', value=p[2])

#     string_expression <- string_expression . f_expression
#     string_expression <- string_expression . fac_expression
def p_stringexpression_a(p):
    '''stringexpression : stringexpression PT fexpression
                        | stringexpression PT facexpression'''
    p[0]=jfrag.atom(obj='function', name='concat', parameters=[p[1], p[3]])

#     string_expression <- f_expression
def p_stringexpression_b(p):
    '''stringexpression : fexpression
                        | facexpression'''
    p[0]=p[1]

#####################    STRING LIST    #####################

#     string_list <- ( string_list_interior )
def p_stringlist_a(p):
    '''stringlist : BRO stringlistinterior BRC'''
    p[0]=p[2]

#####################    STRING LIST INTERIOR    #####################

#     string_list_interior <- string , string_list_interior
def p_stringlistinterior_a(p):
    '''stringlistinterior : STRI STR STRO VIR stringlistinterior'''
    p[0]=[jfrag.atom(obj='str', value=p[2])]+p[5]
    
#     string_list_interior <- string
def p_stringlistinterior_b(p):
    '''stringlistinterior : STRI STR STRO'''
    p[0]=[jfrag.atom(obj='str', value=p[2])]

#####################    ERROR    #####################

def p_error(p):
    print("SYNTAX ERROR IN LINE {}".format(p.lineno))
    yacc.yacc().errok()