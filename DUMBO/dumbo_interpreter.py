import ply.yacc as yacc
from dumbo_syntaxe import *
from dumbo_lex import tokens
from jfrag import pEval,listToText

yacc.yacc(outputdir='generated')
datas=[]
template=[]

if __name__ == "__main__":
    import sys
    input1=open(sys.argv[1]).read()
    input2=open(sys.argv[2]).read()
    datast=yacc.parse(input1,debug=False)
    template=yacc.parse(input2,debug=False)
    print('DATAS EVALUATION ...')
    pEval(datast, datas)
    t=pEval(template,datas)
    print('OUTPUT : ')
    res=listToText(t)
    print(res)
    with open('./output/output.html', 'w') as f:
        f.write(res)
    
